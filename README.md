 - Introduction about yourself
 - Which project to you like most? What and why?
 
## Code quanlity
 - In your opinion what is good code? 
 - How to ensure the code quality of projects
 - How to do code review, how to identify which is good/bad code
 
## Unit test
 - Have you write unit test before?
 - What is unit test
 - What is TDD? RED - GREEN - REFACTOR
    1. Write the test
    2. Run the test (there is no implementation code, test does not pass)
    3. Write just enough implementation code to make the test pass
    4. Run all tests (tests pass)
    5. Refactor
    6. Repeat

## Web basic
- Cookies/How to delete cookies, how to identify a cookies
- How session work

## Security
 - How to secure your app
 - OWASP (2014)
    + Unvalidated Input
    + Broken Access Control
    + Broken Authentication and Session Management
    + Cross Site Scripting (XSS)
    + Buffer Overflows
    + Injection Flaws
    + ...
 
## Performance
 - If your application slow, how do you do to improve it.
 - Experience in solving performance issue, how to scale web sites 

## Foundation
 - Value type vs Reference type
 - OOP
 	+ class vs object
 	+ abstract vs interface
    + override vs overload
 	+ public, proteced, private, internal
	+ static vs non static
    + what is constructor
	
 - SOLID
 - Dependency Injection: how it work, how to manage the lifetime
 
## CSharp
- Boxing vs unboxing  
- First vs FirstOrDefault vs Single
- How can you prevent your class to be inherited further? 
- What is the difference between a class and a structure?
- Generic
- Extenstion method
- Delegate
- What is the function of the Try-Catch-Finally block? 

## MVC
- What is MVC
- When a request come, how MVC handle the request
- ViewBag vs ViewData vs TempData
- Action Filter: What events in action filter
- Model binding
- How to handle authentication
- Routing difference between MVC and WEB API
- HTTP Verbs: GET, PUT, POST, DELETE
- Session/Context management in Nhibernate/EF

## ORM
- What you understand about ORM like how it work
- Lazy loading vs eager  loading
- N + 1

## Database
 - SQL Joins
 - How to write a store procedure with pagination
 - Index in SQL. Index strategy in SQL 
 - ACID: Atomicity, Consistency, Isolation, Durability

## Javascript	
- scope in javascript
- var vs not var
- prototype
- context
- closure
- promise
- cross domain in javascript

## JQuery
- document.ready
- how to select elements by class name
- how to select element by id

## CSS
- box model
- block elements vs inline elements
- positioning
- overflow
- media
- grid system in bootstrap

## Programming
### Scenario 1
Having string = "123213213214325657568689970"
How to find the num of expearence of each 0 to 9
	